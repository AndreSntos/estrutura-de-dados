import java.util.Scanner;

public class Vetor {
    public static void exibeVetor(int[] v) {
        for (int i = 0; i < v.length; i++) {
            System.out.print("v[" + i + "]= " + v[i] + "\t");
        }
        System.out.println();
    }
    public static void exibeVetorContrario(int[] v) {
        for (int i = 6; i >= 0; i--) {
            System.out.print("v[" + i + "]= " + v[i] + "\t");
        }
        System.out.println();
    }
    public static void main(String[] args) {
        int[] vetor = new int[7];
        Scanner leitor = new Scanner(System.in);

        System.out.println("Digite os números que ficaram no seu vetor:");

        for (int i = 0; i < vetor.length; i++){
            vetor[i] = leitor.nextInt();


        }
        exibeVetor(vetor);
        System.out.println();
        exibeVetorContrario(vetor);
    }
}
