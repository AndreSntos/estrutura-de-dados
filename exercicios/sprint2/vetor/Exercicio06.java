import java.util.Scanner;

public class Exercicio06 {
    public static void main(String[] args) {
        Scanner leitor = new Scanner(System.in);

        int[] diasMeses = {31,28,31,30,31,30,31,31,30,31,30,31};
        int numeroDia,numeroMes;

        System.out.println("Digite o dia:");
        numeroDia = leitor.nextInt();
        System.out.println("Digite o mês:");
        numeroMes = leitor.nextInt();

        int totalMes = 0;
        for (int i = 1; i < numeroMes; i++){
            totalMes += diasMeses[i -1];
        }
        System.out.printf("O dia %d/%d corresponde ao dia %d do ano", numeroDia, numeroMes,totalMes + numeroDia);



    }
}
