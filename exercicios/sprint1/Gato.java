public class Gato extends Pet{
//    Atributos


//    Construtor

    public Gato(String nome, Integer rga, String corDoPelo, Double tempoEmAnos, Double tempoEmMeses, String raca, String genero) {
        super(nome, rga, corDoPelo, tempoEmAnos, tempoEmMeses, raca, genero);
    }


//    Métodos

    @Override
    public Double calcIdade() {
        return (getTempoEmAnos() + getTempoEmMeses()) * 15;
    }

    @Override
    public String toString() {
        return "\n Gato: \n" +
                "Idade: " + calcIdade() + "\n" +
                super.toString();
    }
}
