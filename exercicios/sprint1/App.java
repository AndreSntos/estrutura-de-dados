public class App {
    public static void main(String[] args) {
        Gato g = new Gato("Lola",1,"Branco",7.0,0.5,"Vira-lata","Feminino");
        Cachorro c = new Cachorro("Lucky", 2,"Marrom",8.0,0.7,"Yorkshire","Masculino");
        Gato ga = new Gato("Bento",3,"Preto",1.0, 0.2, "Persa","Masculino");
        Petshop p = new Petshop("Pet Mania");

        p.adicionarPet(g);
        p.adicionarPet(c);
        p.adicionarPet(ga);
        System.out.println("\nTodos os Pets:");
        p.exibeTodos();
        System.out.println("\nTodos os gatos:");
        p.exibeGatos();
        System.out.println("\nTodos os filhotes:");
        p.exibeFilhotes();
        System.out.println("\nBuscando pets:");
        p.buscaPet(1);
    }
}
