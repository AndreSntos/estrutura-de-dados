import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

public class Petshop {

//    Atributos
    private String nome;
    private List<Pet> pets;

//    Constructor

    public Petshop(String nome) {
        this.nome = nome;
        this.pets = new ArrayList<>();
    }


//    Métodos

    public void adicionarPet(Pet p){
        pets.add(p);
    }

    public void exibeTodos(){
        for (Pet p: pets) {
            System.out.println(p);
        }
    }

    public void exibeGatos(){
        for (Pet p: pets) {
            if(p instanceof Gato){
                System.out.println(p);
            }
        }
    }

    public void exibeFilhotes(){
        for (Pet p: pets) {
            if(p.calcIdade() <= 25){
                System.out.println(p);
            }
        }
    }

    public void buscaPet(Integer rga){
        Boolean achou = false;
        for (Pet p: pets) {
            if(p.getRga().equals(rga)){
                System.out.println(p);
                achou = true;
            }
        }
        if(achou.equals(false)){
            System.out.println("Desculpe, pet não encontrado");
        }

    }

    @Override
    public String toString() {
        return "Petshop: \n" +
                "Nome: " + nome + '\n';
    }
}
