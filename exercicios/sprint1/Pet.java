public abstract class Pet {
//    Atributos
    private String nome;
    private Integer rga;
    private String corDoPelo;
    private Double tempoEmAnos;
    private Double tempoEmMeses;
    private String raca;
    private String genero;

//    Construtor


    public Pet(String nome, Integer rga, String corDoPelo, Double tempoEmAnos, Double tempoEmMeses, String raca, String genero) {
        this.nome = nome;
        this.rga = rga;
        this.corDoPelo = corDoPelo;
        this.tempoEmAnos = tempoEmAnos;
        this.tempoEmMeses = tempoEmMeses;
        this.raca = raca;
        this.genero = genero;
    }

    //    Métodos
    public abstract Double calcIdade();

    @Override
    public String toString() {
        return  "Nome: " + nome + '\n' +
                "RGA: " + rga + '\n' +
                "Cor do pelo: " + corDoPelo + '\n' +
                "Raça: " + raca + '\n' +
                "Genero: " + genero + '\n';
    }

    public Integer getRga() {
        return rga;
    }

    public Double getTempoEmAnos() {
        return tempoEmAnos;
    }

    public Double getTempoEmMeses() {
        return tempoEmMeses;
    }
}
